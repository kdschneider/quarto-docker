FROM rocker/r2u:jammy

RUN apt -y update && \
    apt -y upgrade && \
    apt -y install \
        cmake \
        curl \
        gdebi-core \
        libcurl4-openssl-dev \
        libfontconfig1-dev \
        libfreetype6-dev \
        libxml2-dev \
        libfribidi-dev \
        libharfbuzz-dev \
        libjpeg-dev \
        libpng-dev \
        libssl-dev \
        libtiff5-dev \
        pandoc \
        pandoc-citeproc \
        && rm -rf /var/lib/apt/lists/*

RUN install.r

RUN curl -LO https://quarto.org/download/latest/quarto-linux-amd64.deb && gdebi --non-interactive quarto-linux-amd64.deb

RUN quarto install tinytex

CMD ["bash"]
